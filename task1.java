package task;
import java.util.ArrayList;
import java.util.List;
public class task1 {
    private static final String letters = "acdegilmnoprstuw";
    public static void main(String[] args) {
        String input = "leepadg";
        System.out.println("input -> " + input);
        long h = hash(input);
        System.out.println("hash -> " + h);
        System.out.println("Output -> " + dehash(h));
    }
    private static String dehash(long hash) {
        List<Character> chars = new ArrayList<>();
        long h = hash;
        while (h > 37) {
            int i = (int) (h % 37);
            chars.add(letters.charAt(i));
            h = h / 37;
        }
        char[] tmp = new char[chars.size()];
        for (int i = chars.size(); i > 0; i--) {
            tmp[chars.size() - i] = chars.get(i - 1);
        }
        String s = new String(tmp);
        return s;
    }
    private static long hash(String input) {
        Long h = 7L;
        for(int i=0; i<input.length(); i++){
            h = h * 37 + letters.indexOf(input.charAt(i));
        }
        return h;
    }
}